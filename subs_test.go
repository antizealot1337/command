package command

import "testing"

var _ Command = (*Subs)(nil)

func TestSubsMatches(t *testing.T) {
	// Create a Subs command
	cmd := Subs{
		Name:  "name",
		Short: "n",
	} //struct

	// Make sure it doesn't match an empty string
	if name := ""; cmd.Matches("") {
		t.Errorf(`Expected cmd to no match "%s"`, name)
	} //if

	// Make sure it matches it's name
	if name := cmd.Name; !cmd.Matches(name) {
		t.Errorf(`Expected cmd to match "%s"`, name)
	} //if

	// Make sure it matches it's (short) name
	if name := cmd.Short; !cmd.Matches(name) {
		t.Errorf(`Expected cmd to match "%s"`, name)
	} //if
} //func

func TestSubsExecute(t *testing.T) {
	// Indicator that command "a" ran
	ranA := false

	// Create a Subs command
	cmd := Subs{
		Cmds: List{
			&Basic{
				Name: "a",
				Run: func([]string) error {
					ranA = true
					return nil
				}, //func
			},
		},
	} //struct

	// Execute with no args
	err := cmd.Execute(nil)

	if err != ErrNoArgs {
		t.Error("Expected error for no args")
	} //if

	// Attempt to run a bad command
	err = cmd.Execute([]string{"", "bad command"})

	// Check for an error
	if err != ErrNoCmd {
		t.Error("Expected error from bad command")
	} //if

	// Attempt to run "a"
	err = cmd.Execute([]string{"", "a"})

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check if "a" was run
	if !ranA {
		t.Error("Expected command to be run but wasn't")
	} //if
} //func
