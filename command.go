package command

// Command represents a command provided at the command line
// (i.e. programname command).
type Command interface {
	Matches(name string) bool
	Execute(args []string) error
} //interface
