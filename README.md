# Command
[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/command?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/command)

A simple package to create simple command line interfaces.

## Why?

There are plenty of CLI packages in the wild. A look at https://go.libhunt.com
or https://github.com/avelino/awesome-go will atest to such. Many will opt for
https://github.com/spf13/cobra and rightly so. What I wanted however was a
simple declarative api for creating commands and executing them rather than
builder patterns or command generators. And thus command was born.

## Usage

*__NOTE:__* The API is still unstable. Expect breaking changes at anytime. You
have been warned.

A simple example with a `command.List`.

```go
package main

import (
  "fmt"
  "os"

  "bitbucket.org/antizealot1337/command"
)

func main() {
  err := cmd.Run(os.Args[1:])

  switch err {
  case nil:
    // All good here
  case command.ErrNoArgs:
    // The user did not provide any command args
  case command.ErrNoCmd:
    // The user provided a command but it didn't match
  default:
    // A command had an error we can handle here
  }
}

var cmds = command.List{
  &command.Basic{
    Name: "greet",
    Desc: "Display a greeting",
    Run:  func(args []string) error {
      if len(args) == 0 {
        return fmt.Errorf("greet error: noone to greet")
      }

      for _, arg := range args {
        fmt.Printf("Hello, %s!\n", arg)
      }

      return nil
    },
  },
  &command.Subs{
    Name:  "enterprise",
    Short: "ent",
    Desc:  "Stuff for the enterpise to do",
    Cmds:  command.List{
      &command.Basic{
        Name: "warp",
        Desc: "Go to warp speed",
        Run:  func([]string) error {
          fmt.Println("She can't take much more cap'n")
          return nil
        },
      },
      &command.Basic{
        Name: "torpedo",
        Desc: "Fire photon torpedo",
        Run:  func([]string) error {
          fmt.Println("Target destroyed")
          return nil
        },
      },
      &command.Basic{
        Name: "hail",
        Desc: "Open a channel",
        Run:  func([]string) error {
          fmt.Println("Hailing frequencies open captain")
          return nil
        },
      },
      &command.Basic{
        Name: "scan",
        Desc: "Run scan",
        Run:  func([]string) error {
          fmt.Println("Captain, I'm picking up some unusual readings")
          return nil
        },
      },
    },
  },
}
```

## License
Licensed under the terms of the MIT license. See LICENSE for more details.
