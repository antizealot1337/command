package command

import "errors"

// ErrExeFail is an error returned from Execute when Command cannot run du to
// an symantic error (usually a missing func for a struct).
var ErrExeFail = errors.New("failed to execute command (no Run)")

// Basic is a basic Command.
type Basic struct {
	Name  string
	Short string
	Desc  string
	Run   func(args []string) error
} //struct

// Matches checks to see if the Basic Command matches the given name.
func (b *Basic) Matches(name string) bool {
	// Check if Short was provided
	if b.Short == "" {
		// Match only on Name
		return b.Name == name
	} //if

	// Match on Name and Short
	return b.Name == name || b.Short == name
} //func

// Execute the Basic Command with the provided args.
func (b *Basic) Execute(args []string) error {
	// Check if Run does not exist
	if b.Run == nil {
		return ErrExeFail
	} //if

	// Call Run
	return b.Run(args)
} //func
