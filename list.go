package command

import (
	"errors"
	"fmt"
)

var (
	// ErrNoArgs is returned when a List attempts to run with no args provided.
	ErrNoArgs = errors.New("no args")

	// ErrNoCmd is returned when a List cannot find a command to run.
	ErrNoCmd = errors.New("command not found")
)

// List is a collection of Commands.
type List []Command

// Run iterates through the List of Commands checking if one is to be run.
func (l List) Run(args []string) error {
	// Check the number of args
	if len(args) == 0 {
		return ErrNoArgs
	} //if

	// Loop through the commands
	for _, cmd := range l {
		// Check if command matches the first arg(name)
		if cmd.Matches(args[0]) {
			// Execute the command
			return cmd.Execute(args[1:])
		} //if
	} //for

	return ErrNoCmd
} //func

// PrintCommands prints the Commands.
func (l List) PrintCommands() {
	// Loop through the commands
	for _, cmd := range l {
		switch cmd := cmd.(type) {
		case *Basic:
			if cmd.Short == "" {
				fmt.Printf(" %s\n", cmd.Name)
			} else {
				fmt.Printf(" %s, %s\n", cmd.Name, cmd.Short)
			} //if
			fmt.Println("\t", cmd.Desc)
		case *Subs:
			if cmd.Short == "" {
				fmt.Printf(" %s <subcommand>\n", cmd.Name)
			} else {
				fmt.Printf(" %s, %s <subcommand>\n", cmd.Name, cmd.Short)
			} //if
			fmt.Println("\t", cmd.Desc)
		default:
		} //switch
	} //for
} //func
