package command

// Subs is a command with sub commands. It takes no action by itself. It only
// tries to executes one of the Cmds.
type Subs struct {
	Name  string
	Short string
	Desc  string
	Cmds  List
} //struct

// Matches checks to see if the Subs Command matches the given name.
func (s *Subs) Matches(name string) bool {
	// Check if Short was provided
	if s.Short == "" {
		// Match only on Name
		return s.Name == name
	} //if

	// Match on Name and Short
	return s.Name == name || s.Short == name
} //func

// Execute the Basic Command with the provided args.
func (s *Subs) Execute(args []string) error {
	// Check if there are no args
	if len(args) == 0 {
		return ErrNoArgs
	} //if

	// Check to run a command
	return s.Cmds.Run(args[1:])
} //func
