package command

import "testing"

func TestListRun(t *testing.T) {
	// Create the commands
	var cmds List = []Command{}

	// Attempt to run without any args
	err := cmds.Run(nil)

	// Make sure an error was provided
	if err == nil && err != ErrNoArgs {
		t.Error("Expected error for no args", err)
	} //if

	// Attempt to run without any args
	err = cmds.Run([]string{""})

	// Make sure an error was provided
	if err == nil && err != ErrNoCmd {
		t.Error("Expected err for no command", err)
	} //if
} //func
