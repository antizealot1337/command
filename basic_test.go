package command

import (
	"errors"
	"testing"
)

var _ Command = (*Basic)(nil)

func TestBasicMatches(t *testing.T) {
	// Create a Basic command
	cmd := Basic{
		Name:  "name",
		Short: "n",
	}

	// Make sure it doesn't match an empty string
	if name := ""; cmd.Matches("") {
		t.Errorf(`Expected cmd to no match "%s"`, name)
	} //if

	// Make sure it matches it's name
	if name := cmd.Name; !cmd.Matches(name) {
		t.Errorf(`Expected cmd to match "%s"`, name)
	} //if

	// Make sure it matches it's (short) name
	if name := cmd.Short; !cmd.Matches(name) {
		t.Errorf(`Expected cmd to match "%s"`, name)
	} //if
} //func

func TestBasicExecute(t *testing.T) {
	// Create a Basic command
	var cmd Basic

	// Attempt to run it without any action
	err := cmd.Execute(nil)

	// Make sure an error was returned
	if err == nil {
		t.Error("Expected error due to missing Run func")
	} //if

	// A flag for the command to change
	flag := false

	// Set the Run func
	cmd.Run = func(args []string) error {
		// Check the number of args
		if expected, actual := 1, len(args); actual != expected {
			t.Fatalf("Expected %d arg(s) but was %d", expected,
				actual)
		} //if

		// Check the first arg
		if expected, actual := "1", args[0]; actual != expected {
			t.Errorf(`Expected first arg to be "%s" but was "%s"`,
				expected, actual)
		} //if

		// Set the flag to true
		flag = true

		return errors.New("")
	} //func

	// Call Execute
	err = cmd.Execute([]string{"1"})

	// Make sure error isn't nil
	if err == nil {
		t.Error("Expected Execute to return an error")
	} //if

	// Check the flag
	if !flag {
		t.Error("Expected Execute to set flag to true")
	} //if
} //func
